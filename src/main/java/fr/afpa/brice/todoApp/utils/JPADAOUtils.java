package fr.afpa.brice.todoApp.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPADAOUtils {

	private static EntityManagerFactory emf = null;

    public JPADAOUtils() {
    }

    public static synchronized EntityManagerFactory getEmf(){
        if(emf == null){
            emf = Persistence.createEntityManagerFactory("gestInscriptionJPA");
        }
        return emf;
    }

    public static synchronized void close(){
        if(emf != null){
            emf.close();
            emf = null;
        }
    }
}
