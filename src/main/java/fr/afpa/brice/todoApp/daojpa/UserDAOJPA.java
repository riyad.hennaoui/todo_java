package fr.afpa.brice.todoApp.daojpa;


import fr.afpa.brice.todoApp.entities.UserEntity;
import fr.afpa.brice.todoApp.model.User;
import fr.afpa.brice.todoApp.utils.JPADAOUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class UserDAOJPA {

    EntityManager entityManager = JPADAOUtils.getEmf().createEntityManager();

    //TODO changer en autre que Entity


    public void insertUser(UserEntity userEntity){


        EntityTransaction transaction = entityManager.getTransaction();
        System.out.println("insertUser");
        transaction.begin();

        entityManager.persist(userEntity);

        transaction.commit();

    }

}
